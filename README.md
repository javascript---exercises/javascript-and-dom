# JavaScript and DOM

Exercise 1

 - Idea was to make a simple JavaScript based web application. Application is supposed to print images and every image has it's own link. By default picture number 1 is printed in screen. You can find the working application from this url https://student.labranet.jamk.fi/~K9452/ttms0500-syksy2019-harjoitukset/Harjoitukset3/Teht%C3%A4v%C3%A4%202.html
 
 Exercise 2
 
  - Idea was to make a simple To Do-list application using JavaScript. In the list you can add things to do and you can also empty the list. Emptying the list is supposed to use addEventListener-method. You can find the working application from this url https://student.labranet.jamk.fi/~K9452/ttms0500-syksy2019-harjoitukset/Harjoitukset3/Teht%C3%A4v%C3%A4%205.html